'use strict';

angular.module('todoApp')
.controller('ListsController', function ListsController($scope, $location, $http) {

	var current_user = JSON.parse(localStorage.getItem('current_user'))
	
	$http({
			url: "http://localhost:3000/api/v1/lists",
			method: "GET",
			params: {user_id: current_user.id}
		})
    .then(function(response) {
        $scope.lists = response.data;
    });

	$scope.submitList = function() {
		$http({
			url: "http://localhost:3000/api/v1/lists",
			method: "POST",
			params: {description: $scope.newList, user_id: current_user.id}
		})
    .then(function(response) {
        // $scope.lists = response.data;
    });

		$scope.lists.push({'description': $scope.newList })
		$scope.newList = ''
	}
});
