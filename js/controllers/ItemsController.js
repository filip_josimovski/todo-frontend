'use strict';

angular.module('todoApp')
.controller('ItemsController', function ItemsController($scope, $location, $http, $routeParams) {

	$scope.items = []

	$scope.makeDone = function(item){
		$http({
			url: "http://localhost:3000/api/v1/items/" + item.id,
			method: "PUT",
			params: {state: "finished"}
		})
    .then(function(response) {
        $scope.items = response.data;
    });
	}

	$http({
			url: "http://localhost:3000/api/v1/items",
			method: "GET",
			params: {list_id: $routeParams.list_id}
		})
    .then(function(response) {
        $scope.items = response.data;
    });

	$scope.submitItem = function() {
		$http({
			url: "http://localhost:3000/api/v1/items",
			method: "POST",
			params: {title: $scope.newItem, list_id: $routeParams.list_id}
		})
    .then(function(response) {
        // $scope.lists = response.data;
    });

		$scope.items.push({'title': $scope.newItem })
		$scope.newItem = ''
	}
});
