// client/app/scripts/controllers/user_sessions.js

'use strict';

angular.module('todoApp')
  .controller('UserSessionsController', ['$rootScope', '$scope', '$http', function ($rootScope, $scope, $http) {

  $rootScope.$on('auth:login-success', function(ev, user) {
	  localStorage.setItem('current_user', JSON.stringify(user))
	  $rootScope.current_user = user
	  alert("Welcome to our app")
	  window.location.href = '/'
	});

	$rootScope.$on('auth:logout-success', function(ev, user) {
	  localStorage.setItem('current_user', null)
	  $rootScope.current_user = null
	  alert("Goodbye")
	  window.location.href = '/'
	});


  $scope.handleRegBtnClick = function() {
      $auth.submitRegistration($scope.registrationForm)
        .then(function(resp) {
          // handle success response
        })
        .catch(function(resp) {
          // handle error response
        });
    };

	$scope.handleLoginBtnClick = function() {
    $auth.submitLogin($scope.loginForm)
      .then(function(resp) {
        $(location).href('/#!/lists')
        // handle success response
      })
      .catch(function(resp) {
        // handle error response
      });
  	};

	$scope.handleSignOutBtnClick = function() {
  	$auth.signOut()
	    .then(function(resp) {
	      // handle success response
	    })
	    .catch(function(resp) {
	      // handle error response
	    });
  };
}]);