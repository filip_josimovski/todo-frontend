'use strict';

angular
  .module('todoApp', [
    'ngRoute',
    'ng-token-auth'
  ])
  .config(function($authProvider) {
    $authProvider.configure({
      apiUrl: 'http://localhost:3000/api/v1'
    });
  })
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainController'
      })
      .when('/login', {
        templateUrl: 'views/user_sessions/new.html',
        controller: 'UserSessionsController',
      })
      .when('/signup', {
        templateUrl: 'views/user_sessions/signup.html',
        controller: 'UserSessionsController'
      })
      .when('/lists', {
        templateUrl: 'views/lists/index.html',
        controller: 'ListsController',
        resolve: {
          auth: function($auth) {
            return $auth.validateUser();
          }
        }
      })
      .when('/lists/items/:list_id', {
        templateUrl: 'views/items/index.html',
        controller: 'ItemsController'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
